<tr>
	<td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
	<td class="text-right"><?php the_field( "cost" ); ?> руб.</td>
	<td class="text-right"><?php the_field( "address" ); ?></td>
	<td class="text-right"><?php the_field( "square" ); ?>м<sup>2</sup></td>
	<td class="text-right"><?php the_field( "life_square" ); ?>м<sup>2</sup></td>
	<td class="text-right"><?php the_field( "floor" ); ?></td>
</tr>