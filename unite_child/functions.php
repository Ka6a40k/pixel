<?php



add_action( 'wp_enqueue_scripts', 'thisajax_data', 99 );
function thisajax_data(){
	wp_enqueue_script('functionscript', get_stylesheet_directory_uri() . '/function.js');
	wp_localize_script( 'functionscript', 'thisajax', 
		array(
			'url' => admin_url('admin-ajax.php')
		)
	);  

}
/* ajax получение контента */
add_action('wp_ajax_ajax_property', 'ajax_property');
add_action('wp_ajax_nopriv_ajax_property', 'ajax_property');
function ajax_property(){
	$agancy = (int)$_POST['agancy'];
	
	if($agancy OR false === ( $all_query = get_transient( 'all_query' ))) {
		$arg = array(
			'post_type'=>'property',
			'post_status'=>'publish',
			'posts_per_page'=>20,
		);
		if($agancy){
			$arg['meta_query']['relation'] = 'AND';
			$arg['meta_query'][] = array('key' => 'agency_offer', 'value' => $agancy);
		}
		
		$all_query = new WP_Query($arg);
		
		set_transient( 'all_query', $all_query, 60*60 );
	}
 	
	if ($all_query->have_posts()){
		while($all_query->have_posts()){ 
			$all_query->the_post();
			get_template_part( 'property' );
		}
		wp_reset_postdata();
	}else{
		echo '<td colspan="6">'._e( 'Извините, нет записей, соответствуюших Вашему запросу.' ).'</td>';
	}
	wp_die();
}
