<?php
/**
 * Template Name: all_posts
 */
get_header(); ?>
<div id="primary" class="content-area col-sm-12 col-md-8">
  <main id="main" class="site-main" role="main">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th>Стоимость</th>
				<th>Адрес</th>
				<th>Площадь</th>
				<th>Жилая площадь</th>
				<th>Этаж</th>
			</tr>
		</thead>
		<tbody id="property_data">
			
		</tbody>
	</table>
  </main><!-- #main -->
</div><!-- #primary -->
<div id="secondary" class="widget-area col-sm-12 col-md-4" role="complementary">

	<?php 
	$arg = array(
		'post_type'=>'agency',
		'post_status'=>'publish',
		'posts_per_page'=>$posts_per_page
	);
	$q = new WP_Query($arg);
	if( $q->have_posts() ):?>
	<ul class="list-group">
	<?php while( $q->have_posts() ): $q->the_post(); ?>
	<li class="list-group-item"><a href="#" onclick="getProperty(<?php the_ID() ?>)"><?php the_title() ?></a></li>
	<?php endwhile; ?>	
	</ul>
	<?php
	endif;
	wp_reset_postdata();
	?>
</div>
<?php get_sidebar(); ?>
<?php get_footer();?>
