jQuery(document).ready(function($){
	getProperty();
});
function getProperty(agancy){
	agancy = agancy?agancy:false;
	jQuery.ajax({
		type: "POST",
    	url: thisajax.url,
    	cache: false,
    	data: {
	        'action': 'ajax_property',
	        'agancy': agancy,
	    	},
    	dataType: "html",
    	success: function(result){
    		jQuery('#property_data').html(result);
    	},
    	error: function(e){
			console.log(e);
		}
	})
}